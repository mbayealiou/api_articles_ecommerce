from flask_sqlalchemy import SQLAlchemy

#Pour gérer les mdp de manière hachée
from werkzeug.security import generate_password_hash, check_password_hash

from flask import render_template, request, jsonify
#from flask_login import login_required,current_user
from sqlalchemy.orm import sessionmaker
import json


# app/article/views.py

from flask import  Flask, abort, flash, redirect, render_template, url_for, jsonify,json,request
from flask_login import current_user, login_required

from flask_uploads import configure_uploads, IMAGES, UploadSet
from werkzeug.utils import secure_filename
import os


app = Flask(__name__)

app.config['SQLALCHEMY_DATABASE_URI'] = 'postgresql://gotputfokxlzmk:676e63adfaa4f08d435a0645736f70f05bdd398c56d3ce47a6eb24bb1532b6a0@ec2-52-22-94-132.compute-1.amazonaws.com:5432/da3rshk329hn02'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
app.secret_key = 'jknjknc6v468v86v354'


db = SQLAlchemy(app)


class Categorie(db.Model):
    """
    Create an Article table
    """

    # Ensures table will be named in plural and not in singular
    # as is the name of the model
    __tablename__ = 'categories'

    id = db.Column(db.Integer, primary_key=True)
    nom = db.Column(db.String(60))
    sexe = db.Column(db.String(60))
    nomsexe = db.Column(db.Text)
    articles = db.relationship('Article', backref='categorie',
                                lazy='dynamic')


    def __repr__(self):
        return '<Categorie: {}>'.format(self.id)




class Article(db.Model):
    """
    Create an Article table
    """

    # Ensures table will be named in plural and not in singular
    # as is the name of the model
    __tablename__ = 'articles'

    id = db.Column(db.Integer, primary_key=True)
    nom = db.Column(db.String(60), index=True)
    description = db.Column(db.Text)
    photo = db.Column(db.Text)
    categorie_id = db.Column(db.Integer,db.ForeignKey('categories.id'))
    date_publication = db.Column(db.Date)
    stocks = db.relationship('Stock', backref='article',
                                lazy='dynamic')


    def __repr__(self):
        return '<Article: {}>'.format(self.nom)



class Stock(db.Model):
    """
    Create an Article table
    """

    # Ensures table will be named in plural and not in singular
    # as is the name of the model
    __tablename__ = 'stocks'

    id = db.Column(db.Integer, primary_key=True)
    id_article = db.Column(db.Integer,db.ForeignKey('articles.id'))
    taille = db.Column(db.String(60))
    nom_couleur = db.Column(db.String(60))
    couleur = db.Column(db.String(60))
    prix_in = db.Column(db.Float)
    prix_fin = db.Column(db.Float)
    solde = db.Column(db.Integer)
    quantite = db.Column(db.Integer)
    photos = db.relationship('Photo', backref='photo',
                                lazy='dynamic')

    def __repr__(self):
        return '<Stock: {}>'.format(self.id)


class Photo(db.Model):
    """
    Create an Article table
    """

    # Ensures table will be named in plural and not in singular
    # as is the name of the model
    __tablename__ = 'photos'

    id = db.Column(db.Integer, primary_key=True)
    id_stock = db.Column(db.Integer, db.ForeignKey('stocks.id'))
    lien_photo = db.Column(db.String(60))
    position = db.Column(db.Integer)


    def __repr__(self):
        return '<Photo: {}>'.format(self.id)


class Avis(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    id_article = db.Column(db.Integer, db.ForeignKey('articles.id'))
    note = db.Column(db.Integer)
    commentaire = db.Column(db.Text)
    id_utilisateur = db.Column(db.Integer)

    def __repr__(self):
        return '<Avis : {}'.format(self.id)

@app.route('/h')
def hello_world():
    return '<h1> Hello AAD </h1>'

@app.route('/test')
def hello_AAD():
    return '<h1> Hello AAD, welcome to test </h1>'

@app.route('/categorie/ajout')
def ajoutCat():
    """
    categorie = Categorie(id=2,nom="pantalon",nomsexe="jean homme",sexe="homme")
    db.session.add(categorie)
    db.session.commit()


    article = Article(id=4,nom="diesel",description="lorem ..",photo = "null",categorie_id=1,date_publication="2020-12-12")
    db.session.add(article)
    db.session.commit()
    """


    article = Article(nom="vans",description="lorem ..",photo = "null",categorie_id=1,date_publication="2020-12-12")
    db.session.add(article)
    db.session.commit()

    """
    stock1 = Stock(id=1,id_article=3,prix_in=12,prix_fin=12,solde=0,nom_couleur="noir",taille="l",quantite=15)
    db.session.add(stock)
    db.session.commit()


    stock = Stock(id=2,id_article=4,prix_in=12,prix_fin=12,solde=0,nom_couleur="noir",taille="l",quantite=15)
    db.session.add(stock)
    db.session.commit()
    """
    """
    avis = Avis(id_article=3,note=12,commentaire="lorem ...")
    db.session.add(avis)
    db.session.commit()
    """
    return "succees"

@app.route('/')
def index():
    articles = Article.query.join(Stock,Stock.id_article==Article.id).add_columns(Article.id,Article.nom,Article.photo,Stock.prix_in,Stock.solde,Stock.prix_fin).limit(20)
    table_articles = []
    for article in articles:
        json_com={
            'id_article': article.id,
            'nom_article': article.nom,
            'prix': article.prix_in,
            'solde': article.solde,
            'prix_fin': article.prix_fin,
            'photo': article.photo
        }
        table_articles.append(json_com)

    categories = Categorie.query.distinct('nom').all()
    table_categories = []
    for categorie in categories:
        json_com={
            'nom': categorie.nom,
            'nom-sexe': categorie.nomsexe,
            'sexe': categorie.sexe
        }
        table_categories.append(json_com)

    marticles = Article.query.join(Stock,Stock.id_article==Article.id).join(Avis,Avis.id_article==Article.id).add_columns(Article.id,Article.nom,Article.photo,Stock.prix_in,Stock.solde,Stock.prix_fin,Avis.note).order_by('note').limit(10)
    table_marticles = []
    for article in marticles:
        json_com={
            'id_article': article.id,
            'nom_article': article.nom,
            'prix': article.prix_in,
            'solde': article.solde,
            'prix_fin': article.prix_fin,
            'photo': article.photo
        }
        table_marticles.append(json_com)

    response = jsonify({"articles": table_articles,'categories': table_categories,'best-seller': table_marticles})
    return response.json

@app.route('/detail/<int:id>')
def detail(id):
    article = Article.query.join(Stock,Stock.id_article==Article.id).add_columns(Article.id,Article.nom,Article.description,Article.photo,Article.categorie_id,Article.description,Stock.prix_in,Stock.taille,Stock.nom_couleur,Stock.couleur,Stock.prix_fin,Stock.solde,Stock.quantite).filter_by(id=id)
    idcat = 1
    for a in article:
        idcat = a.categorie_id

    categories = Categorie.query.filter_by(id=idcat).distinct('nom')
    articles = Article.query.join(Stock,Stock.id_article==Article.id).add_columns(Article.id,Article.nom,Article.description,Article.photo,Article.photo,Stock.prix_in,Stock.taille,Stock.nom_couleur,Stock.couleur,Stock.prix_fin,Stock.solde,Stock.quantite).limit(20).all()
    avis = Avis.query.filter_by(id_article=id)

    table_article = []
    for article in article:
        json_com={
            'id_article': article.id,
            'nom_article': article.nom,
            'prix': article.prix_in,
            'description': article.description,
            'photo': article.photo,
            'couleur': article.nom_couleur,
            'taille': article.taille,
            'solde': article.solde,
            'prix_fin': article.prix_fin,
            'quantite': article.quantite

        }
        table_article.append(json_com)

    table_avis = []
    for av in avis:
        json_com={
            'note': av.note,
            'commentaire': av.commentaire,
            'id_utilisateur': av.id_utilisateur
        }
        table_avis.append(json_com)


    table_articles = []
    for article in articles:
        json_com={
            'id_article': article.id,
            'nom_article': article.nom,
            'prix': article.prix_in,
            'solde': article.solde,
            'prix_fin': article.prix_fin,
            'photo': article.photo
        }
        table_articles.append(json_com)

    table_categories = []
    for categorie in categories:
        json_com={
            'nom_categorie': categorie.nom,
            'nom-sexe': categorie.nomsexe,
            'sexe': categorie.sexe
        }
        table_categories.append(json_com)

    marticles = Article.query.join(Stock,Stock.id_article==Article.id).join(Avis,Avis.id_article==Article.id).add_columns(Article.id,Article.nom,Article.photo,Stock.prix_in,Stock.solde,Stock.prix_fin,Avis.note).order_by('note').limit(10)

    table_marticles = []
    for article in marticles:
        json_com={
            'id_article': article.id,
            'nom_article': article.nom,
            'prix': article.prix_in,
            'solde': article.solde,
            'prix_fin': article.prix_fin,
            'photo': article.photo
        }
        table_marticles.append(json_com)

    response = jsonify({"article": table_article,"articles": table_articles,"categories": table_categories,"avis": table_avis,"best-seller": table_marticles})
    return response.json


@app.route("/recherche/<rech>/",defaults={"ordre": ""})
@app.route("/recherche/<rech>/<ordre>")
def recherche(rech,ordre):
    if request.method == 'POST' or request.method == 'GET':
        if ordre == 'prix':
            articler = Article.query.join(Stock,Stock.id_article==Article.id).add_columns(Article.id,Article.nom,Article.description,Article.photo,Article.description,Stock.prix_in,Stock.taille,Stock.nom_couleur,Stock.couleur,Stock.prix_fin,Stock.solde,Stock.quantite).filter(Article.nom.like("%"+request.form.get('recherche')+"%")).order_by("prix_in").limit(20)
            categorie = Categorie.query.filter_by(id=idcat).distinct('nom')
            articles = Article.query.join(Stock,Stock.id_article==Article.id).join(Avis,Avis.id_article==Article.id).add_columns(Article.id,Article.nom,Article.description,Article.photo,Article.description,Stock.prix_in,Stock.taille,Stock.nom_couleur,Stock.couleur,Stock.prix_fin,Stock.solde,Stock.quantite,Avis.note,Avis.commentaire).order_by('note').limit(20)

            table_articles = []
            for article in articles:
                json_com={
                    'id_article': article.id,
                    'nom_article': article.nom,
                    'prix': article.prix_in,
                    'solde': article.solde,
                    'prix_fin': article.prix_fin,
                    'photo': article.photo
                }
                table_articles.append(json_com)

            table_categories = []
            for categorie in categories:
                json_com={
                    'nom_categorie': categorie.nom,
                    'nom-sexe': categorie.nomsexe,
                    'sexe': categorie.sexe
                }
                table_categories.append(json_com)

            table_articler = []
            for article in articler.items:
                json_com={
                    'id_article': article.id,
                    'nom_article': article.nom,
                    'prix': article.prix_in,
                    'solde': article.solde,
                    'prix_fin': article.prix_fin,
                    'photo': article.photo
                }
                table_articles.append(json_com)

            response = jsonify({"articler": table_articler,"articles": table_articles,"categories": table_categories})
            return response.json

        if ordre == "produit":
            articler = Article.query.join(Stock,Stock.id_article==Article.id).add_columns(Article.id,Article.nom,Article.description,Article.photo,Article.description,Stock.prix_in,Stock.taille,Stock.nom_couleur,Stock.couleur,Stock.prix_fin,Stock.solde,Stock.quantite).filter(Article.nom.like("%"+request.form.get('recherche')+"%")).order_by("nom").limit(20)
            categorie = Categorie.query.filter_by(id=idcat).distinct('nom')
            articles = Article.query.join(Stock,Stock.id_article==Article.id).join(Avis,Avis.id_article==Article.id).add_columns(Article.id,Article.nom,Article.description,Article.photo,Article.description,Stock.prix_in,Stock.taille,Stock.nom_couleur,Stock.couleur,Stock.prix_fin,Stock.solde,Stock.quantite,Avis.note,Avis.commentaire).order_by('note').limit(20)

            table_articles = []
            for article in articles:
                json_com={
                    'id_article': article.id,
                    'nom_article': article.nom,
                    'prix': article.prix_in,
                    'solde': article.solde,
                    'prix_fin': article.prix_fin,
                    'photo': article.photo
                }
                table_articles.append(json_com)

            table_categories = []
            for categorie in categories:
                json_com={
                    'nom_categorie': categorie.nom,
                    'nom-sexe': categorie.nomsexe,
                    'sexe': categorie.sexe
                }
                table_categories.append(json_com)

            table_articler = []
            for article in articler:
                json_com={
                    'id_article': article.id,
                    'nom_article': article.nom,
                    'prix': article.prix_in,
                    'solde': article.solde,
                    'prix_fin': article.prix_fin,
                    'photo': article.photo
                }
                table_articles.append(json_com)

            response = jsonify({"articler": table_articler,"articles": table_articles,"categories": table_categories})
            return response.json

        if ordre == 'note':
            articler = Article.query.join(Stock,Stock.id_article==Article.id).join(Avis,Avis.id_article==Article.id).add_columns(Article.id,Article.nom,Article.description,Article.photo,Article.description,Stock.prix_in,Stock.taille,Stock.nom_couleur,Stock.couleur,Stock.prix_fin,Stock.solde,Stock.quantite,Avis.note).filter(Article.nom.like("%"+request.form.get('recherche')+"%")).order_by("note").limit(20)
            categorie = Categorie.query.filter_by(id=idcat).distinct('nom')
            articles = Article.query.join(Stock,Stock.id_article==Article.id).join(Avis,Avis.id_article==Article.id).add_columns(Article.id,Article.nom,Article.description,Article.photo,Article.description,Stock.prix_in,Stock.taille,Stock.nom_couleur,Stock.couleur,Stock.prix_fin,Stock.solde,Stock.quantite,Avis.note,Avis.commentaire).order_by('note').limit(20)

            table_articles = []
            for article in articles:
                json_com={
                    'id_article': article.id,
                    'nom_article': article.nom,
                    'prix': article.prix_in,
                    'solde': article.solde,
                    'prix_fin': article.prix_fin,
                    'photo': article.photo
                }
                table_articles.append(json_com)

            table_categories = []
            for categorie in categories:
                json_com={
                    'nom_categorie': categorie.nom,
                    'nom-sexe': categorie.nomsexe,
                    'sexe': categorie.sexe
                }
                table_categories.append(json_com)

            table_articler = []
            for article in articler.items:
                json_com={
                    'id_article': article.id,
                    'nom_article': article.nom,
                    'prix': article.prix_in,
                    'solde': article.solde,
                    'prix_fin': article.prix_fin,
                    'photo': article.photo
                }
                table_articles.append(json_com)

            response = jsonify({"articler": table_articler,"articles": table_articles,"categories": table_categories})
            return response.json


    articler = Article.query.join(Stock,Stock.id_article==Article.id).add_columns(Article.id,Article.nom,Article.description,Article.description,Article.photo,Stock.prix_in,Stock.taille,Stock.nom_couleur,Stock.couleur,Stock.prix_fin,Stock.solde,Stock.quantite).filter(Article.nom.like("%"+rech+"%")).limit(20)
    categories = Categorie.query.all()
    articles = Article.query.join(Stock,Stock.id_article==Article.id).join(Avis,Avis.id_article==Article.id).add_columns(Article.id,Article.nom,Article.description,Article.photo,Article.description,Stock.prix_in,Stock.taille,Stock.nom_couleur,Stock.couleur,Stock.prix_fin,Stock.solde,Stock.quantite,Avis.note,Avis.commentaire).order_by('note').limit(20)


    table_articles = []
    for article in articles:
        json_com={
            'id_article': article.id,
            'nom_article': article.nom,
            'prix': article.prix_in,
            'solde': article.solde,
            'prix_fin': article.prix_fin,
            'photo': article.photo
        }
        table_articles.append(json_com)

    table_categories = []
    for categorie in categories:
        json_com={
            'nom_categorie': categorie.nom,
            'nom-sexe': categorie.nomsexe,
            'sexe': categorie.sexe
        }
        table_categories.append(json_com)

    table_articler = []
    for article in articler:
        json_com={
            'id_article': article.id,
            'nom_article': article.nom,
            'prix': article.prix_in,
            'solde': article.solde,
            'prix_fin': article.prix_fin,
            'photo': article.photo
        }
        table_articler.append(json_com)

    response = jsonify({"articler": table_articler,"articles": table_articles,"categories": table_categories})

    return response.json


@app.route("/produits/",defaults={"ordre": ""})
@app.route("/produits/<ordre>",methods=['POST','GET'])
def produits(ordre):
    if (request.method == 'GET' or request.method == 'POST'):
        if ordre == 'prix':
            articles = Article.query.join(Stock,Stock.id_article==Article.id).add_columns(Article.id,Article.nom,Article.description,Article.description,Article.photo,Stock.prix_in,Stock.taille,Stock.nom_couleur,Stock.couleur,Stock.prix_fin,Stock.solde,Stock.quantite).order_by('prix_in').limit(20)
            categories = Categorie.query.distinct('nom').all()
            nbreArticles = 0
            for article in articles:
                nbreArticles = nbreArticles + 1
            table_articles = []
            for article in articles:
                json_com={
                    'id_article': article.id,
                    'nom_article': article.nom,
                    'prix': article.prix_in,
                    'solde': article.solde,
                    'prix_fin': article.prix_fin,
                    'photo': article.photo
                }
                table_articles.append(json_com)

            table_categories = []
            for categorie in categories:
                json_com={
                    'nom_categorie': categorie.nom,
                    'nom-sexe': categorie.nomsexe,
                    'sexe': categorie.sexe
                }
                table_categories.append(json_com)

            nbarticles = {'nombre articles': nbreArticles}

            marticles = Article.query.join(Stock,Stock.id_article==Article.id).join(Avis,Avis.id_article==Article.id).add_columns(Article.id,Article.nom,Article.photo,Stock.prix_in,Stock.solde,Stock.prix_fin,Avis.note).order_by('note').limit(10)
            table_marticles = []
            for article in marticles:
                json_com={
                    'id_article': article.id,
                    'nom_article': article.nom,
                    'prix': article.prix_in,
                    'solde': article.solde,
                    'prix_fin': article.prix_fin,
                    'photo': article.photo
                }
                table_marticles.append(json_com)


            response = jsonify({"articles": table_articles,"categories": table_categories,"nbarticles": nbarticles,"best-seller": table_marticles})
            return response.json


        if ordre == 'produit':
            articles = Article.query.join(Stock,Stock.id_article==Article.id).add_columns(Article.id,Article.nom,Article.description,Article.description,Article.photo,Stock.prix_in,Stock.taille,Stock.nom_couleur,Stock.couleur,Stock.prix_fin,Stock.solde,Stock.quantite).order_by('nom').limit(20)
            categories = Categorie.query.distinct('nom').all()
            nbreArticles = 0
            for article in articles:
                nbreArticles = nbreArticles + 1
            table_articles = []
            for article in articles:
                json_com={
                    'id_article': article.id,
                    'nom_article': article.nom,
                    'prix': article.prix_in,
                    'solde': article.solde,
                    'prix_fin': article.prix_fin,
                    'photo': article.photo
                }
                table_articles.append(json_com)

            table_categories = []
            for categorie in categories:
                json_com={
                    'nom_categorie': categorie.nom,
                    'nom-sexe': categorie.nomsexe,
                    'sexe': categorie.sexe
                }
                table_categories.append(json_com)

            nbarticles = {'nombre articles': nbreArticles}
            marticles = Article.query.join(Stock,Stock.id_article==Article.id).join(Avis,Avis.id_article==Article.id).add_columns(Article.id,Article.nom,Article.photo,Stock.prix_in,Stock.solde,Stock.prix_fin,Avis.note).order_by('note').limit(10)
            table_marticles = []
            for article in marticles:
                json_com={
                    'id_article': article.id,
                    'nom_article': article.nom,
                    'prix': article.prix_in,
                    'solde': article.solde,
                    'prix_fin': article.prix_fin,
                    'photo': article.photo
                }
                table_marticles.append(json_com)

            response = jsonify({"articles": table_articles,"categories": table_categories,"nbarticles": nbarticles,"best-seller": marticles})
            return response.json

        if ordre == 'note':
            articler = Article.query.join(Stock,Stock.id_article==Article.id).join(Avis,Avis.id_article==Article.id).add_columns(Article.id,Article.nom,Article.description,Article.photo,Article.description,Stock.prix_in,Stock.taille,Stock.nom_couleur,Stock.couleur,Stock.prix_fin,Stock.solde,Stock.quantite,Avis.note).filter(Article.nom.like("%"+request.form.get('recherche')+"%")).order_by("note").limit(20)
            categorie = Categorie.query.filter_by(id=idcat).distinct('nom')
            articles = Article.query.join(Stock,Stock.id_article==Article.id).join(Avis,Avis.id_article==Article.id).add_columns(Article.id,Article.nom,Article.description,Article.photo,Article.description,Stock.prix_in,Stock.taille,Stock.nom_couleur,Stock.couleur,Stock.prix_fin,Stock.solde,Stock.quantite,Avis.note,Avis.commentaire).order_by('note').limit(20)

            table_articles = []
            for article in articles:
                json_com={
                    'id_article': article.id,
                    'nom_article': article.nom,
                    'prix': article.prix_in,
                    'solde': article.solde,
                    'prix_fin': article.prix_fin,
                    'photo': article.photo
                }
                table_articles.append(json_com)

            table_categories = []
            for categorie in categories:
                json_com={
                    'nom_categorie': categorie.nom,
                    'nom-sexe': categorie.nomsexe,
                    'sexe': categorie.sexe
                }
                table_categories.append(json_com)

            table_articler = []
            for article in articler.items:
                json_com={
                    'id_article': article.id,
                    'nom_article': article.nom,
                    'prix': article.prix_in,
                    'solde': article.solde,
                    'prix_fin': article.prix_fin,
                    'photo': article.photo
                }
                table_articles.append(json_com)

            response = jsonify({"articler": table_articler,"articles": table_articles,"categories": table_categories})
            return response.json


    articles = Article.query.join(Stock,Stock.id_article==Article.id).add_columns(Article.id,Article.nom,Article.description,Article.description,Article.photo,Stock.prix_in,Stock.taille,Stock.nom_couleur,Stock.couleur,Stock.prix_fin,Stock.solde,Stock.quantite).limit(20)
    categories = Categorie.query.distinct('nom').all()
    nbreArticles = 0
    for article in articles:
        nbreArticles = nbreArticles + 1
    table_articles = []
    for article in articles:
        json_com={
            'id_article': article.id,
            'nom_article': article.nom,
            'prix': article.prix_in,
            'solde': article.solde,
            'prix_fin': article.prix_fin,
            'photo': article.photo
        }
        table_articles.append(json_com)

    table_categories = []
    for categorie in categories:
        json_com={
            'nom_categorie': categorie.nom,
            'nom-sexe': categorie.nomsexe,
            'sexe': categorie.sexe
        }
        table_categories.append(json_com)

    nbarticles = {'nombre articles': nbreArticles}

    marticles = Article.query.join(Stock,Stock.id_article==Article.id).join(Avis,Avis.id_article==Article.id).add_columns(Article.id,Article.nom,Article.photo,Stock.prix_in,Stock.solde,Stock.prix_fin,Avis.note).order_by('note').limit(10)
    table_marticles = []
    for article in marticles:
        json_com={
            'id_article': article.id,
            'nom_article': article.nom,
            'prix': article.prix_in,
            'solde': article.solde,
            'prix_fin': article.prix_fin,
            'photo': article.photo
        }
        table_marticles.append(json_com)


    response = jsonify({"articles": table_articles,"categories": table_categories,"nbarticles": nbarticles,"best-seller": table_marticles})
    return response.json

@app.route("/sexe/<s>/",defaults={'ordre': ""})
@app.route("/sexe/<s>/<ordre>")
def sexe(s,ordre):

    if (request.method == 'POST' or request.method == 'GET'):
        if ordre == 'prix':
            sexe = Categorie.query.filter_by(sexe=s)

            articlec = Stock.query.join(Article,Stock.id_article==Article.id).join(Categorie,Categorie.id==Article.categorie_id).add_columns(Article.id,Article.nom,Article.description,Article.description,Article.photo,Stock.prix_in,Stock.taille,Stock.nom_couleur,Stock.couleur,Stock.prix_fin,Stock.solde,Stock.quantite,Categorie.sexe).filter_by(sexe=s).order_by('prix_in').limit(20)

            s = s

            articles = Article.query.join(Stock,Stock.id_article==Article.id).join(Avis,Avis.id_article==Article.id).add_columns(Article.id,Article.nom,Article.description,Article.description,Article.photo,Stock.prix_in,Stock.taille,Stock.nom_couleur,Stock.couleur,Stock.prix_fin,Stock.solde,Stock.quantite,Avis.note,Avis.commentaire).order_by('note').limit(20)
            nbreArticles = 0

            for article in articlec:
                nbreArticles = nbreArticles + 1

            categories = Categorie.query.distinct('nom').all()

            table_article = []
            for article in articlec:
                json_com={
                    'id_article': article.id,
                    'nom_article': article.nom,
                    'prix': article.prix_in,
                    'solde': article.solde,
                    'prix_fin': article.prix_fin,
                    'photo': article.photo
                }
                table_article.append(json_com)

            table_articles = []
            for article in articles:
                json_com={
                    'id_article': article.id,
                    'nom_article': article.nom,
                    'prix': article.prix_in,
                    'solde': article.solde,
                    'prix_fin': article.prix_fin,
                    'photo': article.photo
                }
                table_articles.append(json_com)

            table_categories = []
            for categorie in categories:
                json_com={
                    'nom_categorie': categorie.nom,
                    'nom-sexe': categorie.nomsexe,
                    'sexe': categorie.sexe
                }
                table_categories.append(json_com)

            nbarticles = {'nombre articles': nbreArticles}

            catégorie_sexe = {'sexe': s}
            response = jsonify({"article": table_article,"articles": table_articles,"categories": table_categories,"nbarticles": nbarticles,"categorie_sexe": categorie_sexe})
            return response.json

        if ordre == 'produit':
            sexe = Categorie.query.filter_by(sexe=s)

            articlec = Stock.query.join(Article,Stock.id_article==Article.id).join(Categorie,Categorie.id==Article.categorie_id).add_columns(Article.id,Article.nom,Article.description,Article.description,Article.photo,Stock.prix_in,Stock.taille,Stock.nom_couleur,Stock.couleur,Stock.prix_fin,Stock.solde,Stock.quantite,Categorie.sexe).filter_by(sexe=s).order_by('prix_in').limit(20)

            s = s

            articles = Article.query.join(Stock,Stock.id_article==Article.id).join(Avis,Avis.id_article==Article.id).add_columns(Article.id,Article.nom,Article.description,Article.photo,Article.description,Stock.prix_in,Stock.taille,Stock.nom_couleur,Stock.couleur,Stock.prix_fin,Stock.solde,Stock.quantite,Avis.note,Avis.commentaire).order_by('note').limit(20)
            nbreArticles = 0
            for article in articlec:
                nbreArticles = nbreArticles + 1

            categories = Categorie.query.distinct('nom').all()

            table_article = []
            for article in articlec:
                json_com={
                    'id_article': article.id,
                    'nom_article': article.nom,
                    'prix': article.prix_in,
                    'solde': article.solde,
                    'prix_fin': article.prix_fin,
                    'photo': article.photo
                }
                table_article.append(json_com)

            table_articles = []
            for article in articles:
                json_com={
                    'id_article': article.id,
                    'nom_article': article.nom,
                    'prix': article.prix_in,
                    'solde': article.solde,
                    'prix_fin': article.prix_fin,
                    'photo': article.photo
                }
                table_articles.append(json_com)

            table_categories = []
            for categorie in categories:
                json_com={
                    'nom_categorie': categorie.nom,
                    'nom-sexe': categorie.nomsexe,
                    'sexe': categorie.sexe
                }
                table_categories.append(json_com)

            nbarticles = {'nombre articles': nbreArticles}
            catégorie_sexe = {'sexe': s}
            response = jsonify({"article": table_article,"articles": table_articles,"categories": table_categories,"nbarticles": nbarticles,"categorie_sexe": categorie_sexe})
            return response.json

        if ordre == 'note':
            articler = Article.query.join(Stock,Stock.id_article==Article.id).join(Avis,Avis.id_article==Article.id).add_columns(Article.id,Article.nom,Article.description,Article.photo,Article.description,Stock.prix_in,Stock.taille,Stock.nom_couleur,Stock.couleur,Stock.prix_fin,Stock.solde,Stock.quantite,Avis.note).filter(Article.nom.like("%"+request.form.get('recherche')+"%")).order_by("note").limit(20)
            categorie = Categorie.query.filter_by(id=idcat).distinct('nom')
            articles = Article.query.join(Stock,Stock.id_article==Article.id).join(Avis,Avis.id_article==Article.id).add_columns(Article.id,Article.nom,Article.description,Article.photo,Article.description,Stock.prix_in,Stock.taille,Stock.nom_couleur,Stock.couleur,Stock.prix_fin,Stock.solde,Stock.quantite,Avis.note,Avis.commentaire).order_by('note').limit(20)

            table_articles = []
            for article in articles:
                json_com={
                    'id_article': article.id,
                    'nom_article': article.nom,
                    'prix': article.prix_in,
                    'solde': article.solde,
                    'prix_fin': article.prix_fin,
                    'photo': article.photo
                }
                table_articles.append(json_com)

            table_categories = []
            for categorie in categories:
                json_com={
                    'nom_categorie': categorie.nom,
                    'nom-sexe': categorie.nomsexe,
                    'sexe': categorie.sexe
                }
                table_categories.append(json_com)

            table_articler = []
            for article in articler.items:
                json_com={
                    'id_article': article.id,
                    'nom_article': article.nom,
                    'prix': article.prix_in,
                    'solde': article.solde,
                    'prix_fin': article.prix_fin,
                    'photo': article.photo
                }
                table_articles.append(json_com)

            response = jsonify({"articler": table_articler,"articles": table_articles,"categories": table_categories})
            return response.json

    sexe = Categorie.query.filter_by(sexe=s)

    articlec = Stock.query.join(Article,Stock.id_article==Article.id).join(Categorie,Categorie.id==Article.categorie_id).add_columns(Article.id,Article.nom,Article.description,Article.description,Article.photo,Stock.prix_in,Stock.taille,Stock.nom_couleur,Stock.couleur,Stock.prix_fin,Stock.solde,Stock.quantite,Categorie.sexe).filter_by(sexe=s).order_by('prix_in').limit(20)

    s = s

    articles = Article.query.join(Stock,Stock.id_article==Article.id).join(Avis,Avis.id_article==Article.id).add_columns(Article.id,Article.nom,Article.description,Article.description,Article.photo,Stock.prix_in,Stock.taille,Stock.nom_couleur,Stock.couleur,Stock.prix_fin,Stock.solde,Stock.quantite,Avis.note,Avis.commentaire).order_by('note').limit(20)
    nbreArticles = 0

    for article in articlec:
        nbreArticles = nbreArticles + 1

    categories = Categorie.query.distinct('nom').all()

    table_article = []
    for article in articlec:
        json_com={
            'id_article': article.id,
            'nom_article': article.nom,
            'prix': article.prix_in,
            'solde': article.solde,
            'prix_fin': article.prix_fin,
            'photo': article.photo
        }
        table_article.append(json_com)

    table_articles = []
    for article in articles:
        json_com={
            'id_article': article.id,
            'nom_article': article.nom,
            'prix': article.prix_in,
            'solde': article.solde,
            'prix_fin': article.prix_fin,
            'photo': article.photo
        }
        table_articles.append(json_com)

    table_categories = []
    for categorie in categories:
        json_com={
            'nom_categorie': categorie.nom,
            'nom-sexe': categorie.nomsexe,
            'sexe': categorie.sexe
        }
        table_categories.append(json_com)

    nbarticles = {'nombre articles': nbreArticles}
    categorie_sexe = {'sexe': s}
    response = jsonify({"article": table_article,"articles": table_articles,"categories": table_categories,"nbarticles": nbarticles,"categorie_sexe": categorie_sexe})

    return response.json


@app.route("/category/<c>/",defaults={'ordre': ""})
@app.route("/category/<c>/<ordre>")
def category(c,ordre):
    if request.method == 'POST' or request.method == 'GET':
        if ordre == 'prix':
            cat = Categorie.query.filter_by(nom=c)
            idcategory = 0
            for a in cat:
                idcategory=a.id

            articlec = Stock.query.join(Article,Stock.id_article==Article.id).add_columns(Article.id,Article.nom,Article.categorie_id,Article.description,Article.photo,Article.description,Stock.prix_in,Stock.taille,Stock.nom_couleur,Stock.couleur,Stock.prix_fin,Stock.solde,Stock.quantite).filter_by(categorie_id=idcategory).order_by('prix_in').limit(20)
            nbreArticles = 0
            for article in articlec:
                nbreArticles = nbreArticles + 1

            articles = Article.query.join(Stock,Stock.id_article==Article.id).join(Avis,Avis.id_article==Article.id).add_columns(Article.id,Article.nom,Article.description,Article.description,Article.photo,Stock.prix_in,Stock.taille,Stock.nom_couleur,Stock.couleur,Stock.prix_fin,Stock.solde,Stock.quantite,Avis.note,Avis.commentaire).order_by('note').limit(20)
            categories = Categorie.query.distinct('nom').all()

            table_article = []
            for article in articlec:
                json_com={
                    'id_article': article.id,
                    'nom_article': article.nom,
                    'prix': article.prix_in,
                    'solde': article.solde,
                    'prix_fin': article.prix_fin,
                    'photo': article.photo
                }
                table_article.append(json_com)

            table_articles = []
            for article in articles:
                json_com={
                    'id_article': article.id,
                    'nom_article': article.nom,
                    'prix': article.prix_in,
                    'solde': article.solde,
                    'prix_fin': article.prix_fin,
                    'photo': article.photo
                }
                table_articles.append(json_com)

            table_categories = []
            for categorie in categories:
                json_com={
                    'nom_categorie': categorie.nom,
                    'nom-sexe': categorie.nomsexe,
                    'sexe': categorie.sexe
                }
                table_categories.append(json_com)

            nbarticles = {'nombre articles': nbreArticles}
            categorie = {'categorie': c}
            response = jsonify({"article": table_article,"articles": table_articles,"categories": table_categories,"nbarticles": nbarticles,"categorie": categorie})

            return response.json

        if ordre == 'produit':
            cat = Categorie.query.filter_by(nom=c)
            idcategory = 0
            for a in cat:
                idcategory=a.id

            articlec = Stock.query.join(Article,Stock.id_article==Article.id).add_columns(Article.id,Article.nom,Article.categorie_id,Article.description,Article.photo,Article.description,Stock.prix_in,Stock.taille,Stock.nom_couleur,Stock.couleur,Stock.prix_fin,Stock.solde,Stock.quantite).filter_by(categorie_id=idcategory).order_by('nom').limit(20)
            nbreArticles = 0
            for article in articlec:
                nbreArticles = nbreArticles + 1

            c = c

            articles = Article.query.join(Stock,Stock.id_article==Article.id).join(Avis,Avis.id_article==Article.id).add_columns(Article.id,Article.nom,Article.description,Article.description,Article.photo,Stock.prix_in,Stock.taille,Stock.nom_couleur,Stock.couleur,Stock.prix_fin,Stock.solde,Stock.quantite,Avis.note,Avis.commentaire).order_by('note').limit(20)
            categories = Categorie.query.distinct('nom').all()

            table_article = []
            for article in articlec:
                json_com={
                    'id_article': article.id,
                    'nom_article': article.nom,
                    'prix': article.prix_in,
                    'solde': article.solde,
                    'prix_fin': article.prix_fin,
                    'photo': article.photo
                }
                table_article.append(json_com)

            table_articles = []
            for article in articles:
                json_com={
                    'id_article': article.id,
                    'nom_article': article.nom,
                    'prix': article.prix_in,
                    'solde': article.solde,
                    'prix_fin': article.prix_fin,
                    'photo': article.photo
                }
                table_articles.append(json_com)

            table_categories = []
            for categorie in categories:
                json_com={
                    'nom_categorie': categorie.nom,
                    'nom-sexe': categorie.nomsexe,
                    'sexe': categorie.sexe
                }
                table_categories.append(json_com)

            nbarticles = {'nombre articles': nbreArticles}
            categorie = {'categorie': c}
            response = jsonify({"article": table_article,"articles": table_articles,"categories": table_categories,"nbarticles": nbarticles,"categorie": categorie})

            return response.json

        if ordre == 'note':
            articler = Article.query.join(Stock,Stock.id_article==Article.id).join(Avis,Avis.id_article==Article.id).add_columns(Article.id,Article.nom,Article.description,Article.photo,Article.description,Stock.prix_in,Stock.taille,Stock.nom_couleur,Stock.couleur,Stock.prix_fin,Stock.solde,Stock.quantite,Avis.note).filter(Article.nom.like("%"+request.form.get('recherche')+"%")).order_by("note").limit(20)
            categorie = Categorie.query.filter_by(id=idcat).distinct('nom')
            articles = Article.query.join(Stock,Stock.id_article==Article.id).join(Avis,Avis.id_article==Article.id).add_columns(Article.id,Article.nom,Article.description,Article.photo,Article.description,Stock.prix_in,Stock.taille,Stock.nom_couleur,Stock.couleur,Stock.prix_fin,Stock.solde,Stock.quantite,Avis.note,Avis.commentaire).order_by('note').limit(20)

            table_articles = []
            for article in articles:
                json_com={
                    'id_article': article.id,
                    'nom_article': article.nom,
                    'prix': article.prix_in,
                    'solde': article.solde,
                    'prix_fin': article.prix_fin,
                    'photo': article.photo
                }
                table_articles.append(json_com)

            table_categories = []
            for categorie in categories:
                json_com={
                    'nom_categorie': categorie.nom,
                    'nom-sexe': categorie.nomsexe,
                    'sexe': categorie.sexe
                }
                table_categories.append(json_com)

            table_articler = []
            for article in articler.items:
                json_com={
                    'id_article': article.id,
                    'nom_article': article.nom,
                    'prix': article.prix_in,
                    'solde': article.solde,
                    'prix_fin': article.prix_fin,
                    'photo': article.photo
                }
                table_articles.append(json_com)

            response = jsonify({"articler": table_articler,"articles": table_articles,"categories": table_categories})
            return response.json

    cat = Categorie.query.filter_by(nom=c)
    idcategory = 0
    for a in cat:
        idcategory=a.id

    articlec = Stock.query.join(Article,Stock.id_article==Article.id).add_columns(Article.id,Article.nom,Article.categorie_id,Article.description,Article.photo,Article.description,Stock.prix_in,Stock.taille,Stock.nom_couleur,Stock.couleur,Stock.prix_fin,Stock.solde,Stock.quantite).filter_by(categorie_id=idcategory).limit(20)
    nbreArticles = 0
    for article in articlec:
        nbreArticles = nbreArticles + 1


    articles = Article.query.join(Stock,Stock.id_article==Article.id).join(Avis,Avis.id_article==Article.id).add_columns(Article.id,Article.nom,Article.description,Article.description,Article.photo,Stock.prix_in,Stock.taille,Stock.nom_couleur,Stock.couleur,Stock.prix_fin,Stock.solde,Stock.quantite,Avis.note,Avis.commentaire).order_by('note').limit(20)
    categories = Categorie.query.all()

    table_article = []
    for article in articlec:
        json_com={
            'id_article': article.id,
            'nom_article': article.nom,
            'prix': article.prix_in,
            'solde': article.solde,
            'prix_fin': article.prix_fin,
            'photo': article.photo
        }
        table_article.append(json_com)

    table_articles = []
    for article in articles:
        json_com={
            'id_article': article.id,
            'nom_article': article.nom,
            'prix': article.prix_in,
            'solde': article.solde,
            'prix_fin': article.prix_fin,
            'photo': article.photo
        }
        table_articles.append(json_com)

    table_categories = []
    for categorie in categories:
        json_com={
            'nom_categorie': categorie.nom,
            'nom-sexe': categorie.nomsexe,
            'sexe': categorie.sexe
        }
        table_categories.append(json_com)

    nbarticles = {'nombre articles': nbreArticles}
    categorie = {'categorie': c}

    response = jsonify({"article": table_article,"articles": table_articles,"categories": table_categories,"nbarticles": nbarticles,"categorie": categorie})

    return response.json

@app.route("/couleur/<color>/",defaults={'ordre': ""})
@app.route('/couleur/<color>/<ordre>')
def couleur(color,ordre):
    if request.method == 'POST' or request.method=='GET':
        if ordre == 'prix':
            nom_couleur = Stock.query.filter_by(nom_couleur=color)
            idart = 0

            for c in nom_couleur:
                idart = c.id_article

            articlec = Stock.query.join(Article,Stock.id_article==Article.id).add_columns(Article.id,Article.nom,Article.description,Article.description,Article.photo,Stock.prix_in,Stock.taille,Stock.nom_couleur,Stock.couleur,Stock.prix_fin,Stock.solde,Stock.quantite).filter_by(id=idart).order_by('prix_in').limit(20)
            nbreArticles = 0
            for article in articlec:
                nbreArticles = nbreArticles + 1

            c = color

            articles = Article.query.join(Stock,Stock.id_article==Article.id).join(Avis,Avis.id_article==Article.id).add_columns(Article.id,Article.nom,Article.description,Article.description,Article.photo,Stock.prix_in,Stock.taille,Stock.nom_couleur,Stock.couleur,Stock.prix_fin,Stock.solde,Stock.quantite,Avis.note,Avis.commentaire).order_by('note').limit(20)
            categories = Categorie.query.distinct('nom').all()

            table_article = []
            for article in articlec:
                json_com={
                    'id_article': article.id,
                    'nom_article': article.nom,
                    'prix': article.prix_in,
                    'solde': article.solde,
                    'prix_fin': article.prix_fin,
                    'photo': article.photo
                }
                table_article.append(json_com)

            table_articles = []
            for article in articles:
                json_com={
                    'id_article': article.id,
                    'nom_article': article.nom,
                    'prix': article.prix_in,
                    'solde': article.solde,
                    'prix_fin': article.prix_fin,
                    'photo': article.photo
                }
                table_articles.append(json_com)

            table_categories = []
            for categorie in categories:
                json_com={
                    'nom_categorie': categorie.nom,
                    'nom-sexe': categorie.nomsexe,
                    'sexe': categorie.sexe
                }
                table_categories.append(json_com)

            nbarticles = {'nombre articles': nbreArticles}
            couleur = {'couleur': c}
            response = jsonify({"article": table_article,"articles": table_articles,"categories": table_categories,"nbarticles": nbarticles,"couleur": couleur})

            return response.json

        if ordre == 'produit':
            nom_couleur = Stock.query.filter_by(nom_couleur=color)
            idart = 0

            for c in nom_couleur:
                idart = c.id_article

            articlec = Stock.query.join(Article,Stock.id_article==Article.id).add_columns(Article.id,Article.nom,Article.description,Article.description,Article.photo,Stock.prix_in,Stock.taille,Stock.nom_couleur,Stock.couleur,Stock.prix_fin,Stock.solde,Stock.quantite).filter_by(id=idart).order_by('nom').limit(20)
            nbreArticles = 0
            for article in articlec:
                nbreArticles = nbreArticles + 1

            c = color

            articles = Article.query.join(Stock,Stock.id_article==Article.id).join(Avis,Avis.id_article==Article.id).add_columns(Article.id,Article.nom,Article.description,Article.description,Article.photo,Stock.prix_in,Stock.taille,Stock.nom_couleur,Stock.couleur,Stock.prix_fin,Stock.solde,Stock.quantite,Avis.note,Avis.commentaire).order_by('note').limit(20)
            categories = Categorie.query.distinct('nom').all()

            table_article = []
            for article in articlec:
                json_com={
                    'id_article': article.id,
                    'nom_article': article.nom,
                    'prix': article.prix_in,
                    'solde': article.solde,
                    'prix_fin': article.prix_fin,
                    'photo': article.photo
                }
                table_article.append(json_com)

            table_articles = []
            for article in articles:
                json_com={
                    'id_article': article.id,
                    'nom_article': article.nom,
                    'prix': article.prix_in,
                    'solde': article.solde,
                    'prix_fin': article.prix_fin,
                    'photo': article.photo
                }
                table_articles.append(json_com)

            table_categories = []
            for categorie in categories:
                json_com={
                    'nom_categorie': categorie.nom,
                    'nom-sexe': categorie.nomsexe,
                    'sexe': categorie.sexe
                }
                table_categories.append(json_com)

            nbarticles = {'nombre articles': nbreArticles}
            couleur = {'couleur': c}
            response = jsonify({"article": table_article,"articles": table_articles,"categories": table_categories,"nbarticles": nbarticles,"couleur": couleur})

            return response.json

        if ordre == 'note':
            articler = Article.query.join(Stock,Stock.id_article==Article.id).join(Avis,Avis.id_article==Article.id).add_columns(Article.id,Article.nom,Article.description,Article.photo,Article.description,Stock.prix_in,Stock.taille,Stock.nom_couleur,Stock.couleur,Stock.prix_fin,Stock.solde,Stock.quantite,Avis.note).filter(Article.nom.like("%"+request.form.get('recherche')+"%")).order_by("note").limit(20)
            categorie = Categorie.query.filter_by(id=idcat).distinct('nom')
            articles = Article.query.join(Stock,Stock.id_article==Article.id).join(Avis,Avis.id_article==Article.id).add_columns(Article.id,Article.nom,Article.description,Article.photo,Article.description,Stock.prix_in,Stock.taille,Stock.nom_couleur,Stock.couleur,Stock.prix_fin,Stock.solde,Stock.quantite,Avis.note,Avis.commentaire).order_by('note').limit(20)

            table_articles = []
            for article in articles:
                json_com={
                    'id_article': article.id,
                    'nom_article': article.nom,
                    'prix': article.prix_in,
                    'solde': article.solde,
                    'prix_fin': article.prix_fin,
                    'photo': article.photo
                }
                table_articles.append(json_com)

            table_categories = []
            for categorie in categories:
                json_com={
                    'nom_categorie': categorie.nom,
                    'nom-sexe': categorie.nomsexe,
                    'sexe': categorie.sexe
                }
                table_categories.append(json_com)

            table_articler = []
            for article in articler.items:
                json_com={
                    'id_article': article.id,
                    'nom_article': article.nom,
                    'prix': article.prix_in,
                    'solde': article.solde,
                    'prix_fin': article.prix_fin,
                    'photo': article.photo
                }
                table_articles.append(json_com)

            response = jsonify({"articler": table_articler,"articles": table_articles,"categories": table_categories})
            return response.json

    nom_couleur = Stock.query.filter_by(nom_couleur=color)
    idart = 0

    for c in nom_couleur:
        idart = c.id_article

    articlec = Stock.query.join(Article,Stock.id_article==Article.id).add_columns(Article.id,Article.nom,Article.description,Article.description,Article.photo,Stock.prix_in,Stock.taille,Stock.nom_couleur,Stock.couleur,Stock.prix_fin,Stock.solde,Stock.quantite).filter_by(id=idart).limit(20)
    nbreArticles = 0
    for article in articlec:
        nbreArticles = nbreArticles + 1

    c = color

    articles = Article.query.join(Stock,Stock.id_article==Article.id).join(Avis,Avis.id_article==Article.id).add_columns(Article.id,Article.nom,Article.description,Article.description,Article.photo,Stock.prix_in,Stock.taille,Stock.nom_couleur,Stock.couleur,Stock.prix_fin,Stock.solde,Stock.quantite,Avis.note,Avis.commentaire).order_by('note').limit(20)
    categories = Categorie.query.distinct('nom').all()

    table_article = []
    for article in articlec:
        json_com={
            'id_article': article.id,
            'nom_article': article.nom,
            'prix': article.prix_in,
            'solde': article.solde,
            'prix_fin': article.prix_fin,
            'photo': article.photo
        }
        table_article.append(json_com)

    table_articles = []
    for article in articles:
        json_com={
            'id_article': article.id,
            'nom_article': article.nom,
            'prix': article.prix_in,
            'solde': article.solde,
            'prix_fin': article.prix_fin,
            'photo': article.photo
        }
        table_articles.append(json_com)

    table_categories = []
    for categorie in categories:
        json_com={
            'nom_categorie': categorie.nom,
            'nom-sexe': categorie.nomsexe,
            'sexe': categorie.sexe
        }
        table_categories.append(json_com)

    nbarticles = {'nombre articles': nbreArticles}
    couleur = {'couleur': c}
    response = jsonify({"article": table_article,"articles": table_articles,"categories": table_categories,"nbarticles": nbarticles,"couleur": couleur})

    return response.json

@app.route("/categorie_couleur/<categ>/<color>/",defaults={'ordre': ""})
@app.route('/categorie_couleur/<categ>/<color>/<ordre>')
def categorie_couleur(categ,color,ordre):
    if request.method == 'POST' or request.method == 'GET':
        if ordre == 'prix':
            nom_couleur = Stock.query.filter_by(nom_couleur=color)
            idart = 0

            for c in nom_couleur:
                idart = c.id_article

            cat = Categorie.query.filter_by(nom=categ)
            idcategory = 0
            for c in cat:
                idcategory=c.id

            articlec = Stock.query.join(Article,Stock.id_article==Article.id).add_columns(Article.id,Article.nom,Article.description,Article.description,Article.photo,Stock.prix_in,Stock.taille,Stock.nom_couleur,Stock.couleur,Stock.prix_fin,Stock.solde,Stock.quantite).filter_by(id=idart).filter_by(categorie_id=idcategory).order_by('prix_in').limit(20)
            nbreArticles = 0
            for article in articlec:
                nbreArticles = nbreArticles + 1

            c = color

            articles = Article.query.join(Stock,Stock.id_article==Article.id).join(Avis,Avis.id_article==Article.id).add_columns(Article.id,Article.nom,Article.description,Article.description,Article.photo,Stock.prix_in,Stock.taille,Stock.nom_couleur,Stock.couleur,Stock.prix_fin,Stock.solde,Stock.quantite,Avis.note,Avis.commentaire).order_by('note').limit(20)
            categories = Categorie.query.distinct('nom').all()

            table_article = []
            for article in articlec:
                json_com={
                    'id_article': article.id,
                    'nom_article': article.nom,
                    'prix': article.prix_in,
                    'solde': article.solde,
                    'prix_fin': article.prix_fin,
                    'photo': article.photo
                }
                table_article.append(json_com)

            table_articles = []
            for article in articles:
                json_com={
                    'id_article': article.id,
                    'nom_article': article.nom,
                    'prix': article.prix_in,
                    'solde': article.solde,
                    'prix_fin': article.prix_fin,
                    'photo': article.photo
                }
                table_articles.append(json_com)

            table_categories = []
            for categorie in categories:
                json_com={
                    'nom_categorie': categorie.nom,
                    'nom-sexe': categorie.nomsexe,
                    'sexe': categorie.sexe
                }
                table_categories.append(json_com)

            nbarticles = {'nombre articles': nbreArticles}
            categorie_couleur = {'categorie': categ,'couleur': c}
            response = jsonify({"article": table_article,"articles": table_articles,"categories": table_categories,"nbarticles": nbarticles,'categorie_couleur': categorie_couleur})

            return response.json


        if ordre == 'produit':
            nom_couleur = Stock.query.filter_by(nom_couleur=color)
            idart = 0

            for c in nom_couleur:
                idart = c.id_article

            cat = Categorie.query.filter_by(nom=categ)
            idcategory = 0
            for c in cat:
                idcategory=c.id

            articlec = Stock.query.join(Article,Stock.id_article==Article.id).add_columns(Article.id,Article.nom,Article.description,Article.description,Article.photo,Stock.prix_in,Stock.taille,Stock.nom_couleur,Stock.couleur,Stock.prix_fin,Stock.solde,Stock.quantite).filter_by(id=idart).filter_by(categorie_id=idcategory).order_by('produit').limit(20)

            nbreArticles = 0
            for article in articlec:
                nbreArticles = nbreArticles + 1

            c = color

            articles = Article.query.join(Stock,Stock.id_article==Article.id).join(Avis,Avis.id_article==Article.id).add_columns(Article.id,Article.nom,Article.description,Article.description,Article.photo,Stock.prix_in,Stock.taille,Stock.nom_couleur,Stock.couleur,Stock.prix_fin,Stock.solde,Stock.quantite,Avis.note,Avis.commentaire).order_by('note').limit(20)
            categories = Categorie.query.distinct('nom').all()


            table_article = []
            for article in articlec:
                json_com={
                    'id_article': article.id,
                    'nom_article': article.nom,
                    'prix': article.prix_in,
                    'solde': article.solde,
                    'prix_fin': article.prix_fin,
                    'photo': article.photo
                }
                table_article.append(json_com)

            table_articles = []
            for article in articles:
                json_com={
                    'id_article': article.id,
                    'nom_article': article.nom,
                    'prix': article.prix_in,
                    'solde': article.solde,
                    'prix_fin': article.prix_fin,
                    'photo': article.photo
                }
                table_articles.append(json_com)

            table_categories = []
            for categorie in categories:
                json_com={
                    'nom_categorie': categorie.nom,
                    'nom-sexe': categorie.nomsexe,
                    'sexe': categorie.sexe
                }
                table_categories.append(json_com)

            nbarticles = {'nombre articles': nbreArticles}
            categorie_couleur = {'categorie': categ,'couleur': c}
            response = jsonify({"article": table_article,"articles": table_articles,"categories": table_categories,"nbarticles": nbarticles,'categorie_couleur': categorie_couleur})

            return response.json

        if ordre == 'note':
            articler = Article.query.join(Stock,Stock.id_article==Article.id).join(Avis,Avis.id_article==Article.id).add_columns(Article.id,Article.nom,Article.description,Article.photo,Article.description,Stock.prix_in,Stock.taille,Stock.nom_couleur,Stock.couleur,Stock.prix_fin,Stock.solde,Stock.quantite,Avis.note).filter(Article.nom.like("%"+request.form.get('recherche')+"%")).order_by("note").limit(20)
            categorie = Categorie.query.filter_by(id=idcat).distinct('nom')
            articles = Article.query.join(Stock,Stock.id_article==Article.id).join(Avis,Avis.id_article==Article.id).add_columns(Article.id,Article.nom,Article.description,Article.photo,Article.description,Stock.prix_in,Stock.taille,Stock.nom_couleur,Stock.couleur,Stock.prix_fin,Stock.solde,Stock.quantite,Avis.note,Avis.commentaire).order_by('note').limit(20)

            table_articles = []
            for article in articles:
                json_com={
                    'id_article': article.id,
                    'nom_article': article.nom,
                    'prix': article.prix_in,
                    'solde': article.solde,
                    'prix_fin': article.prix_fin,
                    'photo': article.photo
                }
                table_articles.append(json_com)

            table_categories = []
            for categorie in categories:
                json_com={
                    'nom_categorie': categorie.nom,
                    'nom-sexe': categorie.nomsexe,
                    'sexe': categorie.sexe
                }
                table_categories.append(json_com)

            table_articler = []
            for article in articler.items:
                json_com={
                    'id_article': article.id,
                    'nom_article': article.nom,
                    'prix': article.prix_in,
                    'solde': article.solde,
                    'prix_fin': article.prix_fin,
                    'photo': article.photo
                }
                table_articles.append(json_com)

            response = jsonify({"articler": table_articler,"articles": table_articles,"categories": table_categories})
            return response.json

    nom_couleur = Stock.query.filter_by(nom_couleur=color)
    idart = 0

    for c in nom_couleur:
        idart = c.id_article

    cat = Categorie.query.filter_by(nom=categ)
    idcategory = 0
    for c in cat:
        idcategory=c.id

    articlec = Stock.query.join(Article,Stock.id_article==Article.id).add_columns(Article.id,Article.nom,Article.description,Article.description,Article.photo,Stock.prix_in,Stock.taille,Stock.nom_couleur,Stock.couleur,Stock.prix_fin,Stock.solde,Stock.quantite).filter_by(id=idart).filter_by(categorie_id=idcategory).limit(20)

    nbreArticles = 0
    for article in articlec:
        nbreArticles = nbreArticles + 1

    c = color

    articles = Article.query.join(Stock,Stock.id_article==Article.id).join(Avis,Avis.id_article==Article.id).add_columns(Article.id,Article.nom,Article.description,Article.description,Article.photo,Stock.prix_in,Stock.taille,Stock.nom_couleur,Stock.couleur,Stock.prix_fin,Stock.solde,Stock.quantite,Avis.note,Avis.commentaire).order_by('note').limit(20)
    categories = Categorie.query.distinct('nom').all()

    table_article = []
    for article in articlec:
        json_com={
            'id_article': article.id,
            'nom_article': article.nom,
            'prix': article.prix_in,
            'solde': article.solde,
            'prix_fin': article.prix_fin,
            'photo': article.photo
        }
        table_article.append(json_com)

    table_articles = []
    for article in articles:
        json_com={
            'id_article': article.id,
            'nom_article': article.nom,
            'prix': article.prix_in,
            'solde': article.solde,
            'prix_fin': article.prix_fin,
            'photo': article.photo
        }
        table_articles.append(json_com)

    table_categories = []
    for categorie in categories:
        json_com={
            'nom_categorie': categorie.nom,
            'nom-sexe': categorie.nomsexe,
            'sexe': categorie.sexe
        }
        table_categories.append(json_com)

    nbarticles = {'nombre articles': nbreArticles}
    categorie_couleur = {'categorie': categ,'couleur': c}
    response = jsonify({"article": table_article,"articles": table_articles,"categories": table_categories,"nbarticles": nbarticles,'categorie_couleur': categorie_couleur})

    return response.json

@app.route("/categorie_sexe/<categ>/<s>/",defaults={'ordre': ""})
@app.route("/categorie_sexe/<categ>/<s>/<ordre>")
def categorie_sexe(categ,s,ordre):
    if request.method == 'POST' or request.method == 'GET':
        if ordre == 'prix':
            sexe = Categorie.query.filter_by(sexe=s).filter_by(nom=categ)

            articlec = ""
            for se in sexe:
                articlec = Stock.query.join(Article,Stock.id_article==Article.id).add_columns(Article.id,Article.nom,Article.description,Article.description,Article.photo,Stock.prix_in,Stock.taille,Stock.nom_couleur,Stock.couleur,Stock.prix_fin,Stock.solde,Stock.quantite).filter_by(categorie_id=se.id).order_by('prix_in').limit(20)



            articles = Article.query.join(Stock,Stock.id_article==Article.id).join(Avis,Avis.id_article==Article.id).add_columns(Article.id,Article.nom,Article.description,Article.description,Article.photo,Stock.prix_in,Stock.taille,Stock.nom_couleur,Stock.couleur,Stock.prix_fin,Stock.solde,Stock.quantite,Avis.note,Avis.commentaire).order_by('note').limit(20)
            nbreArticles = 0
            for article in articlec:
                nbreArticles = nbreArticles + 1
            categories = Categorie.query.distinct('nom').all()

            table_article = []
            for article in articlec:
                json_com={
                    'id_article': article.id,
                    'nom_article': article.nom,
                    'prix': article.prix_in,
                    'solde': article.solde,
                    'prix_fin': article.prix_fin,
                    'photo': article.photo
                }
                table_article.append(json_com)

            table_articles = []
            for article in articles:
                json_com={
                    'id_article': article.id,
                    'nom_article': article.nom,
                    'prix': article.prix_in,
                    'solde': article.solde,
                    'prix_fin': article.prix_fin,
                    'photo': article.photo
                }
                table_articles.append(json_com)

            table_categories = []
            for categorie in categories:
                json_com={
                    'nom_categorie': categorie.nom,
                    'nom-sexe': categorie.nomsexe,
                    'sexe': categorie.sexe
                }
                table_categories.append(json_com)

            nbarticles = {'nombre articles': nbreArticles}
            categorie_sexe = {'categorie': categ,'sexe': s}
            response = jsonify({"article": table_article,"articles": table_articles,"categories": table_categories,"nbarticles": nbarticles,'categorie_sexe': categorie_sexe})

            return response.json

        if ordre == 'produit':
            sexe = Categorie.query.filter_by(sexe=s).filter_by(nom=categ)

            articlec = ""
            for se in sexe:
                articlec = Stock.query.join(Article,Stock.id_article==Article.id).add_columns(Article.id,Article.nom,Article.description,Article.description,Article.photo,Stock.prix_in,Stock.taille,Stock.nom_couleur,Stock.couleur,Stock.prix_fin,Stock.solde,Stock.quantite).filter_by(categorie_id=se.id).order_by('produit').limit(20)



            articles = Article.query.join(Stock,Stock.id_article==Article.id).join(Avis,Avis.id_article==Article.id).add_columns(Article.id,Article.nom,Article.description,Article.description,Article.photo,Stock.prix_in,Stock.taille,Stock.nom_couleur,Stock.couleur,Stock.prix_fin,Stock.solde,Stock.quantite,Avis.note,Avis.commentaire).order_by('note').limit(20)
            nbreArticles = 0
            for article in articlec:
                nbreArticles = nbreArticles + 1
            categories = Categorie.query.distinct('nom').all()

            table_article = []
            for article in articlec:
                json_com={
                    'id_article': article.id,
                    'nom_article': article.nom,
                    'prix': article.prix_in,
                    'solde': article.solde,
                    'prix_fin': article.prix_fin,
                    'photo': article.photo
                }
                table_article.append(json_com)

            table_articles = []
            for article in articles:
                json_com={
                    'id_article': article.id,
                    'nom_article': article.nom,
                    'prix': article.prix_in,
                    'solde': article.solde,
                    'prix_fin': article.prix_fin,
                    'photo': article.photo
                }
                table_articles.append(json_com)

            table_categories = []
            for categorie in categories:
                json_com={
                    'nom_categorie': categorie.nom,
                    'nom-sexe': categorie.nomsexe,
                    'sexe': categorie.sexe
                }
                table_categories.append(json_com)

            nbarticles = {'nombre articles': nbreArticles}
            categorie_sexe = {'categorie': categ,'sexe': s}
            response = jsonify({"article": table_article,"articles": table_articles,"categories": table_categories,"nbarticles": nbarticles,'categorie_sexe': categorie_sexe})

            return response.json

        if ordre == 'note':
            articler = Article.query.join(Stock,Stock.id_article==Article.id).join(Avis,Avis.id_article==Article.id).add_columns(Article.id,Article.nom,Article.description,Article.photo,Article.description,Stock.prix_in,Stock.taille,Stock.nom_couleur,Stock.couleur,Stock.prix_fin,Stock.solde,Stock.quantite,Avis.note).filter(Article.nom.like("%"+request.form.get('recherche')+"%")).order_by("note").limit(20)
            categorie = Categorie.query.filter_by(id=idcat).distinct('nom')
            articles = Article.query.join(Stock,Stock.id_article==Article.id).join(Avis,Avis.id_article==Article.id).add_columns(Article.id,Article.nom,Article.description,Article.photo,Article.description,Stock.prix_in,Stock.taille,Stock.nom_couleur,Stock.couleur,Stock.prix_fin,Stock.solde,Stock.quantite,Avis.note,Avis.commentaire).order_by('note').limit(20)

            table_articles = []
            for article in articles:
                json_com={
                    'id_article': article.id,
                    'nom_article': article.nom,
                    'prix': article.prix_in,
                    'solde': article.solde,
                    'prix_fin': article.prix_fin,
                    'photo': article.photo
                }
                table_articles.append(json_com)

            table_categories = []
            for categorie in categories:
                json_com={
                    'nom_categorie': categorie.nom,
                    'nom-sexe': categorie.nomsexe,
                    'sexe': categorie.sexe
                }
                table_categories.append(json_com)

            table_articler = []
            for article in articler.items:
                json_com={
                    'id_article': article.id,
                    'nom_article': article.nom,
                    'prix': article.prix_in,
                    'solde': article.solde,
                    'prix_fin': article.prix_fin,
                    'photo': article.photo
                }
                table_articles.append(json_com)

            response = jsonify({"articler": table_articler,"articles": table_articles,"categories": table_categories})
            return response.json




    sexe = Categorie.query.filter_by(sexe=s).filter_by(nom=categ)

    articlec = ""
    for se in sexe:
        articlec = Stock.query.join(Article,Stock.id_article==Article.id).add_columns(Article.id,Article.nom,Article.description,Article.description,Article.photo,Stock.prix_in,Stock.taille,Stock.nom_couleur,Stock.couleur,Stock.prix_fin,Stock.solde,Stock.quantite).filter_by(categorie_id=se.id).limit(20)


    articles = Article.query.join(Stock,Stock.id_article==Article.id).join(Avis,Avis.id_article==Article.id).add_columns(Article.id,Article.nom,Article.description,Article.description,Article.photo,Stock.prix_in,Stock.taille,Stock.nom_couleur,Stock.couleur,Stock.prix_fin,Stock.solde,Stock.quantite,Avis.note,Avis.commentaire).order_by('note').limit(20)
    nbreArticles = 0
    for article in articlec:
        nbreArticles = nbreArticles + 1
    categories = Categorie.query.distinct('nom').all()

    table_article = []
    for article in articlec:
        json_com={
            'id_article': article.id,
            'nom_article': article.nom,
            'prix': article.prix_in,
            'solde': article.solde,
            'prix_fin': article.prix_fin,
            'photo': article.photo
        }
        table_article.append(json_com)

    table_articles = []
    for article in articles:
        json_com={
            'id_article': article.id,
            'nom_article': article.nom,
            'prix': article.prix_in,
            'solde': article.solde,
            'prix_fin': article.prix_fin,
            'photo': article.photo
        }
        table_articles.append(json_com)

    table_categories = []
    for categorie in categories:
        json_com={
            'nom_categorie': categorie.nom,
            'nom-sexe': categorie.nomsexe,
            'sexe': categorie.sexe
        }
        table_categories.append(json_com)

    nbarticles = {'nombre articles': nbreArticles}
    categorie_sexe = {'categorie': categ,'sexe': s}
    response = jsonify({"article": table_article,"articles": table_articles,"categories": table_categories,"nbarticles": nbarticles,'categorie_sexe': categorie_sexe})

    return response.json

app.config['UPLOADED_IMAGES_DEST_article'] = '/Users/bayedamethiam/Desktop/STAGE VOLKENO/test1/test/app/static/img/articles'


@app.route('/articles/ajout', methods=['POST'])
def ajoutArticle():

    articlejson = request.get_json()
    article = Article(nom=articlejson['nom'],categorie_id=articlejson['categorie_id'])

    db.session.add(article)
    db.session.commit()

    if ('photo' in articlejson):
        article.photo=articlejson['photo']
        db.session.commit()
        return 'succes'
    json_article={
        'id':article.id
    }
    response = jsonify({"id":json_article})
    return response.json






@app.route('/articles', methods=['GET'])
def liste_article():

    articles = Article.query.all()
    table_article=[]
    for article in articles:
        json_article={
            "id_article": article.id,
            "id_categorie": article.categorie_id,
            "nom":article.nom,
            "date_publication":article.date_publication,
            "photo": article.photo
        }
        table_article.append(json_article)
    #envoi reponse sous forme json
    response = jsonify({"articles":table_article})
    return response.json



@app.route('/articles/sup/<int:id>', methods=['GET','POST'])
def supArticle(id):

    article = Article.query.get_or_404(id)

    db.session.delete(article)
    db.session.commit()

    return 'Suppression effectué avec succès.'





@app.route('/articles/modif/<int:id>', methods=['GET', 'POST'])
def modifArticle(id):



    article=Article.query.get_or_404(id)
    articlejson = request.get_json()
    article.nom = articlejson['nom']
    article.categorie_id = articlejson['id_categorie']
    try:
        article.photo = articlejson['photo']
    except:
        pass




    db.session.commit()



    # redirect to the roles page
    return 'Changement effectué avec succès'








app.config['UPLOADED_IMAGES_DEST'] = '/Users/bayedamethiam/Desktop/STAGE VOLKENO/test1/test/app/static/img/stocks'








@app.route('/stocks/ajout', methods=['GET', 'POST'])
def ajout_stock():


    stockjson = request.get_json()
    article=Article.query.get_or_404(stockjson['id_article'])

    stock = Stock(article=article,taille=stockjson['taille'],couleur=str(stockjson['couleur']),nom_couleur=str(stockjson['nom_couleur']),prix_in=stockjson['prix'],prix_fin=stockjson['prix'] - (stockjson['prix'] *stockjson['solde'] )/100,solde=stockjson['solde'] ,quantite=stockjson['quantite'] )

    db.session.add(stock)
    db.session.commit()

    json_stock={
        'id':stock.id
    }
    response = jsonify({"id":json_stock})
    return response.json




@app.route('/photos/ajout/<int:id>', methods=['GET', 'POST'])
def ajout_photo(id):
    photojson = request.get_json()

    try:
        photo=Photo(id_stock=id,lien_photo=photojson['photo1'] ,position=1)
        db.session.add(photo)
        db.session.commit()
    except :
        pass


    try:

        photo=Photo(id_stock=id,lien_photo=photojson['photo2'],position=2)
        db.session.add(photo)
        db.session.commit()
    except :
        pass


    try:
        photo=Photo(id_stock=id,lien_photo=photojson['photo3'],position=3)
        db.session.add(photo)
        db.session.commit()
    except:
        pass



    try:
        photo=Photo(id_stock=id,lien_photo=photojson['photo4'],position=4)
        db.session.add(photo)
        db.session.commit()
    except:
        pass

    # load department template
    return 'succès'










@app.route('/stocks/<int:id>', methods=['GET', 'POST'])
def list_stock(id):

    table_stock=[]
    stocks=db.session.query(Stock.id,Stock.id_article,Stock.prix_in,Stock.prix_fin,Stock.solde,Stock.taille,Stock.nom_couleur,Stock.couleur,Stock.quantite).filter(Stock.id_article==id).all()

    for stock in stocks:
        json_article={

            "id_stocks":stock.id,
            "id_article": stock.id_article,
            "prix_in":stock.prix_in,
            "prix_fin":stock.prix_fin,
            "solde":stock.solde,
            "taille":stock.taille,
            "nom_couleur":stock.nom_couleur,
            "couleur":stock.couleur,
            "quantite":stock.quantite

        }
        table_stock.append(json_article)
    #envoi reponse sous forme json
    response = jsonify({"stocks":table_stock})
    return response.json




@app.route('/stock/modif/<int:id>', methods=['GET', 'POST'])
def modif_stock(id):
    stock=Stock.query.get_or_404(id)


    stock.prix_in=stockjson['prix']
    stock.prix_fin=stockjson['prix'] - (stockjson['prix'] *stockjson['solde'] )/100
    stock.solde=stockjson['solde']
    stock.taille=stockjson['taille']
    stock.nom_couleur=str(stockjson['nom_couleur'])
    stock.couleur=str(stockjson['couleur'])
    stock.quantite=stockjson['quantite']


    db.session.commit()



    # load department template
    return "succes"



@app.route('/stocks/sup/<int:id>', methods=['GET', 'POST'])
def supStock(id):

    stock = Stock.query.get_or_404(id)

    db.session.delete(stock)
    db.session.commit()


    return "supression reussi"


@app.route('/article/<int:id>', methods=['GET', 'POST'])
def article(id):

    article = Article.query.get_or_404(id)
    json_article={
            "id_article": article.id,
            "nom": article.nom,
            "id_categorie": article.categorie_id,
            "date_publication":article.date_publication,
            "photo": article.photo
    }
    response = jsonify({"article":json_article})
    return response.json



@app.route('/stock/<int:id>', methods=['GET', 'POST'])
def stock(id):

    stock = Stock.query.get_or_404(id)
    json_stock={
            "id_stocks":stock.id,
            "id_article": stock.id_article,
            "prix_in":stock.prix_in,
            "prix_fin":stock.prix_fin,
            "solde":stock.solde,
            "taille":stock.taille,
            "nom_couleur":stock.nom_couleur,
            "couleur":stock.couleur,
            "quantite":stock.quantite
    }
    response = jsonify({"stock":json_stock})
    return response.json





@app.route('/categorie/<int:id>', methods=['GET', 'POST'])
def categorie(id):

    categorie = Categorie.query.get_or_404(id)
    json_categorie={
            "id_categorie":categorie.id,
            "nom": categorie.nom,
            "sexe":categorie.sexe,
            "nomsexe":categorie.nomsexe
    }
    response = jsonify({"categorie":json_categorie})
    return response.json




@app.route('/categories', methods=['GET', 'POST'])
def categories():
    categories = Categorie.query.all()
    table_categorie=[]
    for categorie in categories:
        json_categorie={
            "id_categorie": categorie.id,
            "nom": categorie.nom,
            "sexe":categorie.sexe,
            "nomsexe": categorie.nomsexe
        }
        table_categorie.append(json_categorie)
    #envoi reponse sous forme json
    response = jsonify({"categories":table_categorie})
    return response.json

@app.route('/categories/ajout', methods=['GET', 'POST'])
def ajoutcategories():

    categorie_json = request.get_json()
    categorie = Categorie(nom=categorie_json['nom'],sexe=categorie_json['sexe'],nomsexe=categorie_json['nomsexe'])

    db.session.add(categorie)
    db.session.commit()
    return 'succes'








if __name__ == '__main__':
    db.create_all()
    port = int(os.environ.get('PORT', 5000))
    app.run(debug=True, host='0.0.0.0',port=port)
